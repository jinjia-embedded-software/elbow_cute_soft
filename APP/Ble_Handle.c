#include "Ble_Handle.h"

#define CMD_MAX_QUEUE     5
#define CMD_MAX_LEN       32
#define CMD_REC_SUCCESS   0x55
uint8_t  rec_buf[CMD_MAX_QUEUE][CMD_MAX_LEN];

/*串口接受处理*/
void Ble_Com_Rec_Handle(uint8_t temp,uint8_t out_time_flag)
{
		 #define REC_START_FLAG    0
		 #define REC_CHECK_LEN     1
		 #define REC_CHECK_END     2
		
		 static uint16_t this_cmd_len=0 ; //这条命令的长度
	   static uint16_t have_rec_len=0;
		 static uint8_t rec_state=0;  //接收状态
	   static uint8_t current_queue=0;//目前的队列
		 
		 if(out_time_flag==1) //很久没收到这条命令，相当以前的数据丢弃
		 {
				 rec_state=REC_START_FLAG ;
		 } 
		 
		 if(rec_state==REC_START_FLAG) //开始接收
		 {
				 if(temp==0x0F)
				 {
						 if(current_queue>=CMD_MAX_QUEUE)
						 {
								 current_queue=0;
						 }						  
						 rec_buf[current_queue][0]=temp;
						 have_rec_len=1;
						 rec_state=REC_CHECK_LEN ;
				 }
		 }
		 else if(rec_state==REC_CHECK_LEN ) //判断接收长度
		 {
					rec_buf[current_queue][have_rec_len]=temp;
					have_rec_len+=1;
					if(have_rec_len==2) //接收到2个字节时候计算长度
					{  					
							 this_cmd_len=temp+4;
							 if(this_cmd_len>CMD_MAX_LEN) //长度保护
									rec_state=REC_START_FLAG;
							 else
									rec_state=REC_CHECK_END;
					}
		 }
	   else if(rec_state==REC_CHECK_END) //判断结束
		 {
				rec_buf[current_queue][have_rec_len]=temp;
				have_rec_len+=1;
				if(have_rec_len==this_cmd_len) //长度接收完毕
				{  
					 if(rec_buf[current_queue][this_cmd_len-1]==0xF0) //最后那位是结束码
					 {  
						  rec_buf[current_queue][0]=CMD_REC_SUCCESS;							
						  Ble_Handle();		 
					 }
					 current_queue+=1;
					 rec_state=REC_START_FLAG;
					 
					
				}
		 }
		 #undef CMD_MAX_LEN     
	  
}

void Ble_Handle()
{  
	 static uint8_t idx =0;
	 uint8_t *p;
	
	 //蓝牙命令处理
	 for(uint8_t i=0;i<CMD_MAX_QUEUE;i++)
	  {     
			  if(idx >= CMD_MAX_QUEUE)
				{
				   idx =0;
				}
				
				if(rec_buf[idx][0] == CMD_REC_SUCCESS)
				{  	
            rec_buf[idx][0] = 0x00;			
				    p = rec_buf[idx];
						switch(p[2])
					 {  
						 
					 }
				}
				idx+=1;
	  }
}

