#include "RC_Train.h"

RC_Struct rc_struct;
RC_Struct *rc = &rc_struct;

void Delay(uint16_t s)
{
	 while(s--)
	 {
	    for(uint8_t i=0;i<=10;i++);
	 }
}


void Rc_Train()
{  
	//key=1,肘抬起
   if(rc->key==1)
	 {  
		 //速度从慢到快运动
      for(uint16_t pwm=Pwm_Min;pwm<Pwm_Max;pwm++)
		 {
		 	  Motor2_Set_Pwm(pwm);
			  Delay(10);
			  if(rc->key!=1)
				{
				   break;
				}
		 }
		 
		 while(rc->key==1); //等待按键释放
		 Motor2_Set_Pwm(0);//电机停止
		 Delay(100);//电机停止一段时间才能重新运行
	 }
	 
	 	//key=2,肘下去
   if(rc->key==2)
	 {  
		 //速度从慢到快运动
      for(uint16_t pwm=Pwm_Min;pwm<Pwm_Max;pwm++)
		 {
		 	  Motor2_Set_Pwm(-pwm);
			  Delay(10);
			  if(rc->key!=2)
				{
				   break;
				}
		 }
		 
		 while(rc->key==2); //等待按键释放
		 Motor2_Set_Pwm(0);//电机停止
		 Delay(100);//电机停止一段时间才能重新运行
	 }
	 
	 //key=3,腕左旋
   if(rc->key==3)
	 {  
		 
      Motor_Absolute_Pos(1,Pos_Left,Servo_Speed);
		  while(rc->key==3); //等待按键释放
	    Joint_Set_Enable(1,0);
		  Delay(100);//电机停止一段时间才能重新运行
	 }
	 
	 	//key=4,腕右旋
   if(rc->key==4)
	 {  
     Motor_Absolute_Pos(1,Pos_Right,Servo_Speed);
		 while(rc->key==4); //等待按键释放
	   Joint_Set_Enable(1,0);
		 Delay(100);//电机停止一段时间才能重新运行
	 }
	  
		//key=5,腕顶起
   if(rc->key==5)
	 {  
		 //速度从慢到快运动
      for(uint16_t pwm=Pwm_Min;pwm<Pwm_Max;pwm++)
		 {
		 	  Motor1_Set_Pwm(-pwm);
			  Delay(10);
			  if(rc->key!=5)
				{
				   break;
				}
		 }
		 
		 while(rc->key==5); //等待按键释放
		 Motor1_Set_Pwm(0);//电机停止
		 Delay(100);//电机停止一段时间才能重新运行
	 }
	 
	 	//key=6,腕缩回
   if(rc->key==6)
	 {  
		 //速度从慢到快运动
      for(uint16_t pwm=Pwm_Min;pwm<Pwm_Max;pwm++)
		 {
		 	  Motor1_Set_Pwm(pwm);
			  Delay(10);
			  if(rc->key!=6)
				{
				   break;
				}
		 }
		 
		 while(rc->key==6); //等待按键释放
		 Motor1_Set_Pwm(0);//电机停止
		 Delay(100);//电机停止一段时间才能重新运行
	 }
	 
	 	//key=7,肘部下上，和腕部左右运动
   if(rc->key==7)
	 {  
		  
			//肘部下压，腕部左旋
			for(uint16_t pwm=Pwm_Min;pwm<Pwm_Max;pwm++)
			{
				Motor2_Set_Pwm(-pwm);
				Delay(10);
				if(rc->key!=7)
				{
					 break;
				}
			}
			Motor_Absolute_Pos(1,Pos_Left,Servo_Speed);
      
			//等待运行完成
			M2_Wait_Run_Ok(7);

			//肘部顶起，腕部右旋
			if(rc->key==7)
			{
				for(uint16_t pwm=Pwm_Min;pwm<Pwm_Max;pwm++)
			 {
					Motor2_Set_Pwm(pwm);
					Delay(10);
					if(rc->key!=7)
					{
						 break;
					}
			 }
			 Motor_Absolute_Pos(1,Pos_Right,Servo_Speed);
			}

				//等待运行完成
			M2_Wait_Run_Ok(7);

			//如果按键释放，停止电机
			if(rc->key!=7)
			{  
				Joint_Set_Enable(1,0);//舵机停止
				Motor2_Set_Pwm(0);//电机停止
				Delay(100);//电机停止一段时间才能重新运行
			}
		
	 }
	 
	 /***key=8,肘部下上，和腕部右左运动***/
   if(rc->key==8)
	 {  
		  
			 //肘部下压，腕部右旋
				for(uint16_t pwm=Pwm_Min;pwm<Pwm_Max;pwm++)
			 {
					Motor2_Set_Pwm(-pwm);
					Delay(10);
					if(rc->key!=8)
					{
						 break;
					}
			 }
			 Motor_Absolute_Pos(1,Pos_Right,Servo_Speed);
			 
			 M2_Wait_Run_Ok(8);
			 
			 //肘部顶起，腕部右旋
			 if(rc->key==8)
			 {
					for(uint16_t pwm=Pwm_Min;pwm<Pwm_Max;pwm++)
				 {
						Motor2_Set_Pwm(pwm);
						Delay(10);
						if(rc->key!=8)
						{
							 break;
						}
				 }
				 Motor_Absolute_Pos(1,Pos_Left,Servo_Speed);
			 }
			 
			 M2_Wait_Run_Ok(8);
			 
			 //如果按键释放，停止电机
			 if(rc->key!=8)
			 {  
					Joint_Set_Enable(1,0);//舵机停止
					Motor2_Set_Pwm(0);//电机停止
					Delay(100);//电机停止一段时间才能重新运行
			 }
	 }
	 
}

uint32_t count=0;
void M2_Wait_Run_Ok(uint8_t key)
{
  	//在按键连续按下状态，运动到极限1段时间就退出
		uint32_t time=app->base_time;
		count=0;
		while(rc->key== key)
		{ 
				Delay(10);
				if(ADC_Get_Avr_Value(1)<5)
				{
					 count+=1;
				}
				else
				{
					 count =0;
				}
				if(count>20000)
			 {
					 Motor2_Set_Pwm(0);
					 Delay(100);
					 break;
			 }
			 
			 if((app->base_time - time)>=8*Second) //限制只能运行8S
			 {
					 Motor2_Set_Pwm(0);
			 }
		}
}
