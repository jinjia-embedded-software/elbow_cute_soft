#ifndef __APP_H__
#define __APP_H__
#include "L6205.h"
#include "RC_Train.h"
#include "ADC1.h"

//定义常量
#define Second 100

//app结构体定义
typedef struct{
	 uint32_t base_time;//系统基准时间
}App_Struct;

extern App_Struct  *app;

//对外函数
void App_10ms_Refresh(void);
void App_Run(void);

#endif
