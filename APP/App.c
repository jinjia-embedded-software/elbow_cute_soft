#include "App.h"
App_Struct  app_struct;
App_Struct  *app = &app_struct;

/*10ms刷新处理*/
uint16_t adc0,adc1;
void App_10ms_Refresh()
{
  
	 Led_Handle(10);//led指示灯
	 rc->key = M433_Get_State();
	 app->base_time +=1;
	 adc0 = ADC_Get_Avr_Value(0);
	 adc1 = ADC_Get_Avr_Value(1);
}

/*主程序处理*/
void App_Run(void)
{
   Rc_Train();
}


void App_Init()
{
    Motor2_Set_Pwm(-Pwm_Max);
	  Motor_Absolute_Pos(1,Pos_Origin,Servo_Speed);
	  for(uint32_t i=0;i<=0x2ffffff;i++);
	  Joint_Set_Enable(1,0);
}

