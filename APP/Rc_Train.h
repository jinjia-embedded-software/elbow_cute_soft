#ifndef __Beidong_Train_H__
#define __Beidong_Train_H__
#include "App.h"
#include "M433.h"
#include "Multi_Joint_Protocol.h"

#define Pos_Left     2048-1024
#define Pos_Right    1024+2048
#define Pos_Origin   1024
#define Servo_Speed  500

#define Pwm_Max  2300
#define Pwm_Min  500
//被动训练模式结构体
typedef struct{
	uint8_t key;
}RC_Struct;

extern RC_Struct *rc;
void Rc_Train(void);
void App_Init(void);

/*电机2等待运行ok*/
static void M2_Wait_Run_Ok(uint8_t key);
#endif

