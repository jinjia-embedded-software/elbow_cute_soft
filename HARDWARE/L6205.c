#include "L6205.h"

Motor_Struct   motor_struct;
Motor_Struct  *motor = &motor_struct;

/*Motor_Io_Config配置*/
static void Motor_Io_Config(void)
{
   GPIO_InitTypeDef GPIO_InitStructure;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	 GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;	
	
	 GPIO_InitStructure.GPIO_Pin = M1_En_Pin;
	 GPIO_Init(M1_En_GPIO, &GPIO_InitStructure);
	 
	 GPIO_InitStructure.GPIO_Pin = M2_En_Pin;
	 GPIO_Init(M2_En_GPIO, &GPIO_InitStructure);
	 
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
   GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	
	 GPIO_InitStructure.GPIO_Pin = M1_In1_Pin;
	 GPIO_Init(M1_In1_GPIO, &GPIO_InitStructure);
	 
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

	 GPIO_InitStructure.GPIO_Pin = M1_In2_Pin;
	 GPIO_Init(M1_In2_GPIO, &GPIO_InitStructure);
	
	 GPIO_InitStructure.GPIO_Pin = M2_In1_Pin;
	 GPIO_Init(M2_In1_GPIO, &GPIO_InitStructure);
	 
	 GPIO_InitStructure.GPIO_Pin = M2_In2_Pin;
	 GPIO_Init(M2_In2_GPIO, &GPIO_InitStructure);
}


void TIM3_PWM_Config(void)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  TIM_OCInitTypeDef  TIM_OCInitStructure;
  

  GPIO_PinAFConfig(GPIOB, GPIO_PinSource4,GPIO_AF_1);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource5,GPIO_AF_1);
	
  /* TIM1 时钟使能 */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , ENABLE);
  /* Time 定时基础设置*/
  TIM_TimeBaseStructure.TIM_Period = DCM_TIM_Period;//周期
  TIM_TimeBaseStructure.TIM_Prescaler = DCM_TIM_Prescaler;//分频系数
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//设置时钟分割系数：不分割
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  ////向上计数模式
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
 
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
  TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
  TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;
  TIM_OCInitStructure.TIM_Pulse = 0;
	
  TIM_OC1Init(TIM3, &TIM_OCInitStructure);
  TIM_OC2Init(TIM3, &TIM_OCInitStructure);
	
	TIM_ARRPreloadConfig(TIM3, ENABLE);			 // 使能TIM3重载寄存器ARR
  TIM_Cmd(TIM3, ENABLE);
  
}

static void TIM1_PWM_Config()
{
  //时钟初始化结构体
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
  
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10,GPIO_AF_2);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource11,GPIO_AF_2);
	
	 /* TIM1 时钟使能 */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);
	
	//设置时钟TIM1
  TIM_TimeBaseStructure.TIM_Period =DCM_TIM_Period;    //周期
  TIM_TimeBaseStructure.TIM_Prescaler =DCM_TIM_Prescaler; //分频系数
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;	//设置时钟分割系数：不分割
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式
  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); //初始化时钟
	
	//时钟通道初始化
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;	    //配置为PWM模式1
  TIM_OCInitStructure.TIM_OutputState =TIM_OutputState_Enable;	//
	TIM_OCInitStructure.TIM_OutputNState=TIM_OutputNState_Disable;//禁止互补输出
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;  //当定时器计数值小于CCR1_Val时为高电平
	TIM_OCInitStructure.TIM_OCNPolarity=TIM_OCNPolarity_Low;//设置互补输出的极性
	TIM_OCInitStructure.TIM_OCIdleState=TIM_OCIdleState_Set;//
  TIM_OCInitStructure.TIM_OCNIdleState=TIM_OCNIdleState_Reset;//
  TIM_OCInitStructure.TIM_Pulse =0;	   //设置跳变值，当计数器计数到这个值时，电平发生跳变，默认是0，低电平输出
 

	TIM_OC3Init(TIM1, &TIM_OCInitStructure) ;
  TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);//使能TIM1的通道3	

	TIM_OC4Init(TIM1, &TIM_OCInitStructure) ;
  TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Enable);//使能TIM1的通道4
  
	TIM_CtrlPWMOutputs(TIM1,ENABLE);	   //MOE 主输出使能	
	TIM_ARRPreloadConfig(TIM1, ENABLE);	 // 使能TIM1重载寄存器ARR
	TIM_Cmd(TIM1, ENABLE);               //使能TIM1	
}

void Motor_Config()
{
   Motor_Io_Config();
	 TIM3_PWM_Config();
	 TIM1_PWM_Config();
	 Motor1_EN();
	 Motor2_EN();
//	  Motor1_Set_Pwm(1800);
//		Motor2_Set_Pwm(1800);
//	while(1)
//	{
//	    Motor1_Set_Pwm(50);
//		  Motor2_Set_Pwm(50);
//		  for(uint32_t i=0;i<=0x1ffffff;i++);
//		  Motor1_Set_Pwm(0);
//		  Motor2_Set_Pwm(0);
//		  for(uint32_t i=0;i<=0xfffff;i++);
//		  Motor1_Set_Pwm(-50);
//		  Motor2_Set_Pwm(-50);
//		  for(uint32_t i=0;i<=0x1ffffff;i++);
//	}
}


void Motor1_Set_Pwm(int16_t pwm)
{  	 
	 if(pwm>=0)
	 {
	     Motor1_IN2_Set_Pwm(0) ;
		   Motor1_IN1_Set_Pwm(pwm) ;
	 }
	 else
	 {   
		   Motor1_IN1_Set_Pwm(0);
		   Motor1_IN2_Set_Pwm(-pwm) ;
	 }
}

void Motor2_Set_Pwm(int16_t pwm)
{  	 
	 if(pwm>=0)
	 {
	     Motor2_IN2_Set_Pwm(0) ;
		   Motor2_IN1_Set_Pwm(pwm) ;
	 }
	 else
	 {   
		   Motor2_IN1_Set_Pwm(0);
		   Motor2_IN2_Set_Pwm(-pwm) ;
	 }
}


