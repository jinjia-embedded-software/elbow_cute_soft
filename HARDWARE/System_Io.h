#ifndef __System_IO_H
#define __System_IO_H
#include "stm32f0xx_gpio.h"

/*****COM1的IO端口配置*****/
#define  COM1_Tx_GPIO                 GPIOB
#define  COM1_Tx_Pin                  GPIO_Pin_6
#define  COM1_Rx_GPIO                 GPIOB
#define  COM1_Rx_Pin                  GPIO_Pin_7

/*****COM3的IO端口配置*****/
#define  COM2_Tx_GPIO                 GPIOA
#define  COM2_Tx_Pin                  GPIO_Pin_2
#define  COM2_Rx_GPIO                 GPIOA
#define  COM2_Rx_Pin                  GPIO_Pin_3

/****485管脚控制***/
#define Rs485_Ctl_GPIO                GPIOB
#define Rs485_Ctl_Pin                 GPIO_Pin_8

/****电机****/
#define  M1_En_GPIO                   GPIOB 
#define  M1_En_Pin                    GPIO_Pin_3
#define  M1_In1_GPIO                  GPIOB 
#define  M1_In1_Pin                   GPIO_Pin_4
#define  M1_In2_GPIO                  GPIOB 
#define  M1_In2_Pin                   GPIO_Pin_5
#define  M2_En_GPIO                   GPIOA 
#define  M2_En_Pin                    GPIO_Pin_9
#define  M2_In1_GPIO                  GPIOA 
#define  M2_In1_Pin                   GPIO_Pin_10
#define  M2_In2_GPIO                  GPIOA 
#define  M2_In2_Pin                   GPIO_Pin_11

#define  M1_Current_GPIO              GPIOB 
#define  M1_Current_Pin               GPIO_Pin_0
#define  M2_Current_GPIO              GPIOB 
#define  M2_Current_Pin               GPIO_Pin_1

/*蓝牙*/
#define  Ble_Sta_GPIO                 GPIOA
#define  Ble_Sta_Pin                  GPIO_Pin_4
#define  Ble_At_En_GPIO               GPIOA
#define  Ble_At_En_Pin                GPIO_Pin_5
#define  Ble_Sleep_GPIO               GPIOA 
#define  Ble_Sleep_Pin                GPIO_Pin_6

/*无线通信*/
#define  Dat433_GPIO                  GPIOB
#define  Dat433_Pin                   GPIO_Pin_12

/*****LED的IO端口配置*****/
#define  Led_GPIO                     GPIOA
#define  Led_Pin                      GPIO_Pin_1

#endif

