#ifndef __Ble_H
#define __Ble_H
#include "stm32f0xx.h"
#include "System_Io.h"

#define Ble_Con_State        //蓝牙连接状态
#define Ble_DisCon           //蓝牙断开状态

#define Get_Ble_Sta_State()      GPIO_ReadInputDataBit( Ble_Sta_GPIO,  Ble_Sta_Pin)

#define Ble_Sleep()               Ble_Sleep_GPIO->BSRR =Ble_Sleep_Pin           //休眠,高电平
#define Ble_No_Sleep()            Ble_Sleep_GPIO->BRR = Ble_Sleep_Pin           //不休眠，低电平

#define Ble_At_En()               Ble_At_En_GPIO->BRR =Ble_At_En_Pin            //AT模式,低电平
#define Ble_At_Dis()              Ble_At_En_GPIO->BSRR = Ble_At_En_Pin           //透传模式，高电平


void  Ble_Config(void); //Ble配置

#endif
