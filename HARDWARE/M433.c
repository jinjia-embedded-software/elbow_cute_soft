#include "M433.h"

M433_Struct  m433_struct;
M433_Struct  *m433 = &m433_struct;

/*TIM2定时器配置*/
static void TIM2_Config()
{
  //初始化的结构体  
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  NVIC_InitTypeDef NVIC_InitStructure; 
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	
	//中断配置
  NVIC_InitStructure.NVIC_IRQChannel =TIM2_IRQn; 
  NVIC_InitStructure.NVIC_IRQChannelPriority =0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
	//设置时钟TIM2
  TIM_TimeBaseStructure.TIM_Period =TIM2_Period;    //周期
  TIM_TimeBaseStructure.TIM_Prescaler =TIM2_Prescaler; //分频系数
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;	//设置时钟分割系数：不分割
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //初始化时钟
		
	TIM_UpdateRequestConfig(TIM2,TIM_UpdateSource_Regular); //更新请求源设为只能是计数器溢出
  TIM_SetCounter(TIM2, 0x0000); 
  TIM_ClearFlag(TIM2, TIM_FLAG_Update);        //清除更新标志位
  TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update); //清除TIM2等待中断更新中断标志位
  TIM_ARRPreloadConfig(TIM2, ENABLE);        //预装载寄存器的内容被立即传送到影子寄存器
	
  TIM_Cmd(TIM2, ENABLE); //使能TIM2	
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE); //使能TIM2的中断	
}

//Dat管脚初始化
static void M433_Dat_Exit_Init()
{
  EXTI_InitTypeDef EXTI_InitStructure;		//EXTIx中断线配置
	NVIC_InitTypeDef NVIC_InitStructure;		//EXTIx中断向量配置
 
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;  //配置成输入
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;	 //上拉
	GPIO_InitStructure.GPIO_Pin = Dat433_Pin;
	GPIO_Init(Dat433_GPIO, &GPIO_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG,ENABLE);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource12);	//将EXTI2指向PB12
	//配置dat引脚为上升沿和下降源都中断
	EXTI_InitStructure.EXTI_Line=EXTI_Line12;
	EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Rising_Falling;
	EXTI_InitStructure.EXTI_LineCmd=ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	NVIC_InitStructure.NVIC_IRQChannel=EXTI4_15_IRQn ;
	NVIC_InitStructure.NVIC_IRQChannelPriority=0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/* 433配置*/
void M433_Config(void)
{
   TIM2_Config();
	 M433_Dat_Exit_Init();
	 //M433_Rec_Cmd(0);
	 
	//M433_Rec_Cmd(1);
//	 while(1);
}

void M433_Rec_Cmd(uint8_t flag)
{  
	 static uint8_t previous_state= 0xff;
	 if(previous_state == flag)
	 {
	    return;
	 }
	 previous_state = flag;
	 
	 EXTI_InitTypeDef EXTI_InitStructure;		//EXTIx中断线配置
   if(flag == 0)
	 {  
		  TIM_Cmd(TIM2, DISABLE); //使能TIM2	
			EXTI_DeInit();
	 }
	 else
	 {  
		  m433->rec_data = 0;
	    m433->start_flag =0;	 
		  TIM_Cmd(TIM2, ENABLE); //使能TIM2	
		 //配置dat引脚为上升沿和下降源都中断
			EXTI_InitStructure.EXTI_Line=EXTI_Line12;
			EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
			EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Rising_Falling;
			EXTI_InitStructure.EXTI_LineCmd=ENABLE;
			EXTI_Init(&EXTI_InitStructure);
	 }
}

void TIM2_IRQHandler(void) 
{
  if (((TIM2->SR & TIM_IT_Update) != (uint16_t)RESET) && (TIM2->DIER & TIM_IT_Update)!= (uint16_t)RESET)
  {     
		  TIM2->SR =0; //清除中断	  
		  m433->base_time += 1;
		  //LED_Turn() ;
	}
}


//433 dat_pin的中断处理
uint16_t interval;
uint16_t err_count;
uint32_t temp_rec_data;
//单位为100us
#define Start_Min_Time  100 
#define Start_Max_Time  110 
#define One_Min_Time    10 
#define One_Max_Time    11 
#define Zero_Min_Time   3 
#define Zero_Max_Time   4 

void EXTI4_15_IRQHandler()
{  
	 //EXTI_ClearFlag(EXTI_Line14);				//退出中断时清除标志
	 EXTI->PR = EXTI_Line12;
	 
	 uint16_t pin_level = Dat433_GPIO->IDR & Dat433_Pin;

	 start:
	 if(m433->start_flag == 0)
	 {
			 if (pin_level==0) //下降沿
			 {
					 m433 ->falling_start_time = m433->base_time;
			 }
			 else //上升沿
			 {
					interval = m433->base_time - m433 ->falling_start_time;
					if(interval>= Start_Min_Time && interval<= Start_Max_Time)
					{
							m433->start_flag = 1;
						  m433->have_rec_bit = 0;
						  temp_rec_data=0;
						  err_count =0;
					}
					else
          {   
						  err_count +=1;
						  if(err_count>=30)
							{
							    m433->rec_data = 0;
							}
					}
			 }
   }
	 
	 if(m433->start_flag == 1)
	 {
	     if (pin_level!=0) //上升沿
			 {
					 m433 ->falling_start_time = m433->base_time;
			 }
			 else //下降沿
			 {
					interval = m433->base_time - m433 ->falling_start_time;
					if(interval>= One_Min_Time && interval <= One_Max_Time)  //高电平在这个范围内为1码
					{  
						  m433->have_rec_bit +=1;
						  temp_rec_data += 1<<(26 - m433->have_rec_bit);					 
					}
					else if(interval>= Zero_Min_Time && interval<= Zero_Max_Time) //高电平在这个范围内为0码
					{
						  m433->have_rec_bit +=1;
					}
					else //码不在协议范围内强制重新接收;
					{
  					  m433->start_flag = 0;
						  return;
					}
					
					if(m433->have_rec_bit>=26) //一帧数据为26位
					{
					   m433->start_flag = 0;
						 m433->rec_data = temp_rec_data;
						 goto start;
					}
			 }
	 }
}

//按键判断

uint8_t  M433_Get_State()
{
    uint8_t keys[8]={0xA1,0x91,0xB1,0x89,0xA9,0x99,0xB9,0x85};
	  uint8_t key= m433->rec_data;
	  for(uint8_t i=0;i<8;i++)
		{
		   if(key == keys[i])
			 {
			    return i+1;
			 }
		}
    return  0;
}





