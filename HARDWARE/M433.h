#ifndef __M433_H
#define __M433_H
#include "stm32f0xx.h"
#include "System_Io.h"
#include "LED.h"

#define TIM2_Period                    (uint16_t)(99)  //定时器周期
#define TIM2_Prescaler                 (uint16_t)(47)     //预分频值         
#define TIM2_NVIC_IRQChannel           TIM2_IRQn        //TIM2中断源


typedef struct{
	 uint32_t base_time;
	 uint32_t falling_start_time;
	 uint8_t start_flag;
	 uint32_t rec_data;
	 uint32_t have_rec_bit;
}M433_Struct;

void M433_Config(void); //433配置
void M433_Rec_Cmd(uint8_t flag);//
uint8_t  M433_Get_State(void);

static void TIM3_Config(void);//
static void M433_Dat_Exit_Init(void);//Dat管脚初始化
#endif
