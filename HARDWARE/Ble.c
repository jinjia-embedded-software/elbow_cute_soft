#include "Ble.h"

/*LED����*/
void Ble_Config(void)
{
   GPIO_InitTypeDef GPIO_InitStructure;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	 GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;	
	 //En
	 GPIO_InitStructure.GPIO_Pin = Ble_At_En_Pin;
	 GPIO_Init(Ble_At_En_GPIO, &GPIO_InitStructure);
	//Sleep
	 GPIO_InitStructure.GPIO_Pin = Ble_Sleep_Pin;
	 GPIO_Init(Ble_Sleep_GPIO, &GPIO_InitStructure);
  
	//sta����
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	 GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	
	 GPIO_InitStructure.GPIO_Pin = Ble_Sta_Pin;
	 GPIO_Init(Ble_Sta_GPIO, &GPIO_InitStructure);
	 
	 Ble_No_Sleep();
	 //Ble_Sleep();
	 //Ble_At_Dis();
	
	 Ble_At_En();
}



