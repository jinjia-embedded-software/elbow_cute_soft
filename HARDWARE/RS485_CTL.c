#include "RS485_CTL.h"

/*
 * 函数名：RS485_CTL_Pin_Config
 * 描述  ：RS485_CTL的GPIO,工作模式配置
 * 输入  ：无
 * 输出  : 无
 * 调用  ：内部调用
 */
static void RS485_CTL_Pin_Config()
{  
	 GPIO_InitTypeDef GPIO_InitStructure;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	 GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;	
	
	 GPIO_InitStructure.GPIO_Pin = Rs485_Ctl_Pin;
	 GPIO_Init(Rs485_Ctl_GPIO, &GPIO_InitStructure);
}

/*
 * 函数名：RS485_CTL_Config
 * 描述  ：RS485_CTL的配置
 * 输入  ：无
 * 输出  : 无
 * 调用  ：内部调用
 */
void RS485_CTL_Config()
{  
	 RS485_CTL_Pin_Config();
	 RS485_Set_Receive();
}

