#ifndef __L6205_H
#define __L6205_H
#include "stm32f0xx.h"
#include "System_Io.h"

#define DCM_TIM_Period                              2000                      //定时器周期
#define DCM_TIM_Prescaler                           0     

#define Motor1_EN()   						        M1_En_GPIO->BSRR = M1_En_Pin;
#define Motor1_Disable()   						    M1_En_GPIO->BRR = M1_En_Pin;
#define Motor1_IN1_Set_Pwm(pwm)           TIM3->CCR1=(uint16_t)pwm 
#define Motor1_IN2_Set_Pwm(pwm)           TIM3->CCR2=(uint16_t)pwm 

#define Motor2_EN()   						        M2_En_GPIO->BSRR = M2_En_Pin;
#define Motor2_Disable()   						    M2_En_GPIO->BRR = M2_En_Pin;
#define Motor2_IN1_Set_Pwm(pwm)           TIM1->CCR3=(uint16_t)pwm 
#define Motor2_IN2_Set_Pwm(pwm)           TIM1->CCR4=(uint16_t)pwm 

//电机结构体
typedef struct{
	uint8_t  state;
}Motor_Struct;


extern Motor_Struct  *motor ;

void Motor_Config(void); //Motor配置
void Motor1_Set_Pwm(int16_t pwm);
void Motor2_Set_Pwm(int16_t pwm);
#endif
