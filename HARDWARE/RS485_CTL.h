#ifndef __RS485_CTL_H
#define	__RS485_CTL_H
#include "stm32f0xx.h"
#include <stdio.h>
#include "System_IO.h"

#define RS485_Set_Send()                           Rs485_Ctl_GPIO->BSRR =Rs485_Ctl_Pin          //COM3_4851设置为发送，高电平
#define RS485_Set_Receive()                        Rs485_Ctl_GPIO->BRR = Rs485_Ctl_Pin          //COM3_4851设置为接收，低电平

void RS485_CTL_Config(void);

#endif 


