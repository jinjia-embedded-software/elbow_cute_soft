#ifndef __LED_H__
#define __LED_H__
#include "stm32f0xx.h"
#include "System_Io.h"

#define LED_On()                           Led_GPIO->BRR =Led_Pin            //LED亮，低电平
#define LED_Off()                          Led_GPIO->BSRR = Led_Pin          //LED灭，高电平
#define LED_Turn()                         Led_GPIO->ODR^=Led_Pin            //LED翻转

void Led_Config(void); //LED配置
void Led_Handle(uint16_t xms);

#endif
