#include "TIM14.h"

static void (*Timer14_CallBack)(void);

/*TIM14定时器配置*/
void TIM14_Config(void (*fun)(void))
{
  //初始化的结构体  
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  NVIC_InitTypeDef NVIC_InitStructure; 
	
	Timer14_CallBack=fun;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
	
	//中断配置
  NVIC_InitStructure.NVIC_IRQChannel =TIM14_IRQn; 
  NVIC_InitStructure.NVIC_IRQChannelPriority =3;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
	//设置时钟TIM14
  TIM_TimeBaseStructure.TIM_Period =TIM14_Period;    //周期
  TIM_TimeBaseStructure.TIM_Prescaler =TIM14_Prescaler; //分频系数
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;	//设置时钟分割系数：不分割
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式
  TIM_TimeBaseInit(TIM14, &TIM_TimeBaseStructure); //初始化时钟
		
	TIM_UpdateRequestConfig(TIM14,TIM_UpdateSource_Regular); //更新请求源设为只能是计数器溢出
  TIM_SetCounter(TIM14, 0x0000); 
  TIM_ClearFlag(TIM14, TIM_FLAG_Update);        //清除更新标志位
  TIM_ClearITPendingBit(TIM14, TIM_FLAG_Update); //清除TIM14等待中断更新中断标志位
  TIM_ARRPreloadConfig(TIM14, ENABLE);        //预装载寄存器的内容被立即传送到影子寄存器
	
  TIM_Cmd(TIM14, ENABLE); //使能TIM14	
	TIM_ITConfig(TIM14,TIM_IT_Update,ENABLE); //使能TIM14的中断	
}


void TIM14_IRQHandler(void) 
{
  if (((TIM14->SR & TIM_IT_Update) != (uint16_t)RESET) && (TIM14->DIER & TIM_IT_Update)!= (uint16_t)RESET)
  {     
		  TIM14->SR =0; //清除中断	  
		  Timer14_CallBack();
	}
}































