#include "ADC1.h"
static void Bubble_Sort(uint16_t *value,uint16_t len);

ADC_Struct adc_struct; 
ADC_Struct *adc=&adc_struct; //ADC结构体

/*adc io初始化*/
static void ADC1_IO_Config()
{
		GPIO_InitTypeDef    GPIO_InitStruct;
		GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
		GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	
		GPIO_InitStruct.GPIO_Pin = M1_Current_Pin ;
		GPIO_Init(M1_Current_GPIO, &GPIO_InitStruct);
	  GPIO_InitStruct.GPIO_Pin = M2_Current_Pin ;
		GPIO_Init(M2_Current_GPIO, &GPIO_InitStruct);
}

/*DMA初始化*/
static void ADC1_DMA_Config(void)
{
  ADC_InitTypeDef     ADC_InitStruct;
	DMA_InitTypeDef     DMA_InitStruct;
	 
	/* ADC1 DeInit */
	ADC_DeInit(ADC1);        
	 
	/* ADC1 Periph clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);  
	/* DMA1 clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1 , ENABLE); 

	RCC_ADCCLKConfig(RCC_ADCCLK_PCLK_Div4);  
			          				 
	/* DMA1 Channel1 Config */
	DMA_DeInit(DMA1_Channel1);
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&(ADC1->DR);  
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)adc->samle_result;
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralSRC;                                                                                
	DMA_InitStruct.DMA_BufferSize = ADC_SAMPLE_COUNT  ;                                                                                                                                
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;                       
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;                                                        
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;       
	DMA_InitStruct.DMA_Mode = DMA_Mode_Circular;                                                                                       
	DMA_InitStruct.DMA_Priority = DMA_Priority_High;                                                                       
	DMA_InitStruct.DMA_M2M = DMA_M2M_Disable;                                                                                                        
	DMA_Init(DMA1_Channel1, &DMA_InitStruct);

	/* DMA1 Channel1 enable */
	DMA_Cmd(DMA1_Channel1, ENABLE);                                                                                                                                               
	/*Enables the specified DMAy Channelx interrupts */
 //DMA_ITConfig(DMA1_Channel1,DMA_IT_TC,ENABLE);                                                                                       
  /* ADC DMA request in circular mode */
	ADC_DMARequestModeConfig(ADC1, ADC_DMAMode_Circular);                                                        
  /* Enable ADC_DMA */
	ADC_DMACmd(ADC1, ENABLE);
	 
	/* Initialize ADC structure */
	ADC_StructInit(&ADC_InitStruct);
	 
	/* Configure the ADC1 in continous mode withe a resolutuion equal to 12 bits  */
	ADC_InitStruct.ADC_Resolution  = ADC_Resolution_12b;
	ADC_InitStruct.ADC_ContinuousConvMode  = ENABLE;                          //模数转换工作在循环转换模式
	ADC_InitStruct.ADC_ExternalTrigConvEdge  = ADC_ExternalTrigConvEdge_None; //转换由软件而不是外部触发启动
	ADC_InitStruct.ADC_DataAlign  = ADC_DataAlign_Right;                      //ADC数据右对齐
	ADC_InitStruct.ADC_ScanDirection  = ADC_ScanDirection_Backward;
	ADC_Init(ADC1, &ADC_InitStruct);  

	ADC_ChannelConfig(ADC1, ADC_Channel_8, ADC_SampleTime_239_5Cycles);        
	ADC_ChannelConfig(ADC1, ADC_Channel_9, ADC_SampleTime_239_5Cycles);    
	ADC_GetCalibrationFactor(ADC1); //adc校准
	ADC_Cmd(ADC1, ENABLE);   /* Enable ADC1 */    
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY));
	
	ADC_StartOfConversion(ADC1);  /*ADC1 regular Software Start Conv */
}

 /*ADC_Config的配置*/
void ADC_Config(void) 
{
   ADC1_IO_Config();
	 ADC1_DMA_Config();
}

/*
 * 函数名：ADC_Get_Avr_Value
 * 描述  ：得到ADC某通道的平均值
 * 输入  ：ADC_Idx是adc的通道值
 * 输出  : ADC某通道的平均值
 * 调用  ：外部调用
 */
uint16_t ADC_Get_Avr_Value(uint8_t adc_idx)
{ 
	uint16_t i,start_index,end_index;
	uint16_t value[ADC_SAMPLE_COUNT];//原始数据缓冲区
	uint32_t sum=0; //采用截取中间数值的和
	
	for(i=0;i<ADC_SAMPLE_COUNT;i++)//复制DMA采用地址区的数据出来
	{
	   value[i]=adc->samle_result[i][adc_idx]; 
	}
	Bubble_Sort(value,ADC_SAMPLE_COUNT);//冒泡法从小到大排序
	
	start_index=ADC_SAMPLE_COUNT/4;
	end_index=ADC_SAMPLE_COUNT*3/4;
	for(i=start_index;i<end_index;i++)//取中间一半的数值算平均数
	{
	  sum+=value[i];
	}

	return sum/(end_index-start_index);
}

/*
*  函数名: Bubble_Sort
 * 描述  ：对传进来的数据进行从小到大排列
 * 输入  ：*value是数据的首地址,len是数据的长度
 * 输出  : 无
 * 调用  ：内部调用
 */
static void Bubble_Sort(uint16_t *value,uint16_t len)
{  
	 uint16_t i,j,temp;
		for (i = 0; i <(len-1); i++)
		{
			for ( j=(i+1); j < len; j++)
			{
					if (value[i] > value[j])
					{
							temp =value[i];
							value[i] = value[j];
 							value[j] = temp;
					}
			}
		}
}






