#include "COMM2.h"

Com2_Struct com2_struct; //串口1的结构体
Com2_Struct *com2=&com2_struct; //串口1的结构体

void COM2_Config(void)
{
		GPIO_InitTypeDef  GPIO_InitStructure;
		USART_InitTypeDef USART_InitStructure;
		NVIC_InitTypeDef 	NVIC_InitStructure;
	
	  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE );		
	
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource2,GPIO_AF_1);
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource3,GPIO_AF_1);     											 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2|GPIO_Pin_3;                 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; 
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitStructure);   
    
		USART_InitStructure.USART_BaudRate = COM2_BandRate ;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_No;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
		USART_Init(USART2, &USART_InitStructure);
		
		NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = 2;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
		
		USART_Cmd(USART2, ENABLE);
		USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
//		USART_ITConfig(USART2,USART_IT_ORE, ENABLE);
}

/*COM2直接发送*/
void COM2_Dir_Send(uint8_t *pbuf, uint16_t len)
{   
    while(len--)
    {   
			  USART_SendData(USART2,*pbuf++);
        while(USART_GetFlagStatus(USART2,USART_FLAG_TC)!=SET);
    }
}


void USART2_IRQHandler(void)
{   
	 volatile uint8_t temp;
   if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
   {    
		    USART_ClearITPendingBit(USART2,USART_IT_RXNE);
		  	temp =USART_ReceiveData(USART2); 
		    if(com2->no_rec_time>20)
				{   
					  Ble_Com_Rec_Handle(temp,1);		  
				}
				else
				{  
						Ble_Com_Rec_Handle(temp,0); 	
				}  
			  com2->no_rec_time=0;
    }
	  //else if(USART_GetITStatus(COM2, USART_IT_ORE) == SET )	
		else if(USART_GetFlagStatus(COM2, USART_FLAG_ORE) == SET )	//没开溢出中断需要用USART_GetFlagStatus，而开溢出中断会自动开启帧错误等中
		{
		    USART_ClearITPendingBit(USART2, USART_IT_ORE);
		}
}

