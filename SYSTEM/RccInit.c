#include "RCCInit.h"

void System_Clock_Configuration()
{
  #if(GPIOA_CLOCK_EN) //设置端口A时钟
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);
	#endif
	
	#if(GPIOB_CLOCK_EN) //设置端口B时钟
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB,ENABLE);
	#endif
	
	#if(GPIOC_CLOCK_EN) //设置端口C时钟
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC,ENABLE);
	#endif
	
	#if(GPIOD_CLOCK_EN) //设置端口D时钟
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD,ENABLE);
	#endif
	
	#if(GPIOF_CLOCK_EN) //设置端口F时钟
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOF,ENABLE);
	#endif	
}



