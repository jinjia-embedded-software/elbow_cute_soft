#include "COMM1.h"


Com1_Struct com1_struct; //串口1的结构体
Com1_Struct *com1=&com1_struct; //串口1的结构体

/*串口初始化*/
void COM1_Config(void)
{
		GPIO_InitTypeDef  GPIO_InitStructure;
		USART_InitTypeDef USART_InitStructure;
		NVIC_InitTypeDef 	NVIC_InitStructure;
	
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE );	
		GPIO_PinAFConfig(GPIOB,GPIO_PinSource6,GPIO_AF_0);
		GPIO_PinAFConfig(GPIOB,GPIO_PinSource7,GPIO_AF_0);     											 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;                 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; 
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOB, &GPIO_InitStructure);   

		USART_InitStructure.USART_BaudRate = COM1_BandRate ;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_No;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
		USART_Init(USART1, &USART_InitStructure);
		
		NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = 3;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
		
		USART_Cmd(USART1, ENABLE);
		USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
//		USART_ITConfig(USART1,USART_IT_ORE, ENABLE);
}


/*串口发送*/
void COM1_Dir_Send(uint8_t *pbuf, uint16_t len)
{   
	  RS485_Set_Send();
    while(len--)
    {   
			  USART_SendData(USART1,*pbuf++);
        while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
    }
		for(uint16_t i=0;i<=0x320;i++);
		RS485_Set_Receive();
}



void USART1_IRQHandler(void)
{   
	 uint8_t temp;
   if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
   {    
		    USART_ClearITPendingBit(USART1,USART_IT_RXNE);
		  	temp =USART_ReceiveData(USART1); 
   
//		    if(com1->no_rec_time>20)
//				{   
//					  Ble_Com_Rec_Handle(temp,1);		  
//				}
//				else
//				{  
//						Ble_Com_Rec_Handle(temp,0); 	
//				}  
			  com1->no_rec_time=0;
    }
	  //else if(USART_GetITStatus(COM1, USART_IT_ORE) == SET )	
		else if(USART_GetFlagStatus(COM1, USART_FLAG_ORE) == SET )	//没开溢出中断需要用USART_GetFlagStatus，而开溢出中断会自动开启帧错误等中断
		{
		    USART_ClearITPendingBit(USART1, USART_IT_ORE);
		}
}


