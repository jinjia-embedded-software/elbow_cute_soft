#ifndef __ADC_H
#define __ADC_H

#include "stm32f0xx.h"
#include "System_IO.h"

#define ADC_SAMPLE_COUNT                  ((uint8_t)10)                        //ADC1每通道采样次数
#define ADC_CH_NUM                        2                                     //ADC1开启的采用通道数
/*****ADC的结构体*****/
typedef struct{
	uint16_t samle_result[ADC_SAMPLE_COUNT][ADC_CH_NUM];  //采样的结果
}ADC_Struct;

void ADC_Config(void);
uint16_t ADC_Get_Avr_Value(uint8_t adc_idx);

#endif
