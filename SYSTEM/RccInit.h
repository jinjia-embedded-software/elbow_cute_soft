#ifndef __RCCInit_H
#define	__RCCInit_H

#include "stm32f0xx.h"

#define GPIOA_CLOCK_EN     1u       //确定是否开启GPIOA时钟 
#define GPIOB_CLOCK_EN     1u       //确定是否开启GPIOB时钟 
#define GPIOC_CLOCK_EN     1u       //确定是否开启GPIOC时钟 
#define GPIOD_CLOCK_EN     0u       //确定是否开启GPIOD时钟 
#define GPIOF_CLOCK_EN     1u       //确定是否开启GPIOF时钟 

void System_Clock_Configuration(void);
#endif
