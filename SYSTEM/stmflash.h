#ifndef __STMFLASH_H__
#define __STMFLASH_H__
#include "stm32f0xx.h" 
#include "stm32f0xx_flash.h"  
#include "System_Data.h"
#include "sys.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////
//用户根据自己的需要设置
#define STM32_FLASH_SIZE MCU_FLASH_SIZE	 		      //所选STM32的FLASH容量大小(单位为K)
#define STM32_FLASH_BASE 0x08000000 	  //STM32 FLASH的起始地址

#if STM32_FLASH_SIZE<256 //定义一个扇区的大小，FLASH小于256K的是1K，FLASH大于256K的是2K
#define STM_SECTOR_SIZE 1024 //
#else 
#define STM_SECTOR_SIZE	2048
#endif	

//////////////////////////////////////////////////////////
u16 STMFLASH_ReadHalfWord(u32 faddr);		  //读出半字
u8 STMFLASH_Read_Byte(u32 faddr); //读出
void STMFLASH_WriteLenByte(u32 WriteAddr,u32 DataToWrite,u16 Len);	//指定地址开始写入指定长度的数据
u32 STMFLASH_ReadLenByte(u32 ReadAddr,u16 Len);						//指定地址开始读取指定长度数据
void STMFLASH_Write(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite);		//从指定地址开始写入指定长度的数据
void STMFLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead);   		//从指定地址开始读出指定长度的数据

						   
#endif

















