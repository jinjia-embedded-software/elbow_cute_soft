#ifndef __COMM1_H
#define	__COMM1_H
#include "stm32f0xx.h"
#include "System_Io.h"
#include "Ble_Handle.h"
#include "RS485_CTL.h"

/*****COM1相关配置*****/
#define COM1                                      USART1                                    // 所用到的串口
#define COM1_BandRate                            ((uint32_t)115200)                           //波特率
#define COM1_RX_LEN                              ((uint16_t)64)                              //接收的字节长度,建议多几个字节

/*****COM1的结构体*****/
typedef struct{
	 uint32_t no_rec_time;
	 uint8_t rx_buf[COM1_RX_LEN];
	 uint8_t rx_state;
}Com1_Struct;

/*****对外变量*****/
extern Com1_Struct *com1;

/*****对外方法*****/
void COM1_Config(void);
void COM1_Dir_Send(uint8_t *pbuf, uint16_t len);
#endif
