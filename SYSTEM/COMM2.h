#ifndef __COMM2_H
#define	__COMM2_H

#include "stm32f0xx.h"
#include "System_Io.h"
#include "RS485_CTL.h"
#include "Ble_Handle.h"

/*****COM1相关配置*****/
#define COM2                                      USART2                                    // 所用到的串口
#define COM2_BandRate                            ((uint32_t)115200)                           //波特率
#define COM2_RX_LEN                              ((uint16_t)64)                              //接收的字节长度,建议多几个字节

/*****COM2的结构体*****/
typedef struct{
	 uint32_t no_rec_time;
	 uint8_t rx_buf[COM2_RX_LEN];
	 uint8_t rx_state;
}Com2_Struct;

/*****对外变量*****/
extern Com2_Struct *com2;

/*****对外方法*****/
void COM2_Config(void);
void COM2_Dir_Send(uint8_t *pbuf, uint16_t len);
#endif
