#ifndef __TIM14_H
#define	__TIM14_H
#include "stm32f0xx.h"

//TIM14_PWM配置 10ms
#define TIM14_Period                    (uint16_t)(9999)  //定时器周期
#define TIM14_Prescaler                 (uint16_t)(47)     //预分频值         
#define TIM14_NVIC_IRQChannel           TIM14_IRQn        //TIM14中断源


void TIM14_Config(void (*fun)(void));//TIM14时钟的初始化

#endif
