#include "stm32f0xx.h"
#include "RCCInit.h"
#include "COMM1.h"
#include "COMM2.h"
#include "ADC1.h"
#include "TIM14.h"

#include "RS485_CTL.h"
#include "L6205.h"
#include "Ble.h"
#include "M433.h"
#include "Led.h"

#include "App.h"
#include "Multi_Joint_Protocol.h"

static void System_Dev_Config(void);

uint8_t buf[3]={1,2,3};
/*主函数*/
int main(void)
{  
	 System_Dev_Config(); //硬件设备初始化
	
   while(1)
 	 { 
		  App_Run();//App处理
	 } 
}

/*设备初始化*/
static void System_Dev_Config()
{   
	 SystemInit();//配置系统时钟为48M;
	 System_Clock_Configuration(); //使能用到的功能接口时钟
	 for(uint32_t i=0;i<0xffff;i++);
	 Motor_Config();
	
   RS485_CTL_Config();  
	 COM1_Config();
	 COM2_Config();
	 ADC_Config();
	 Led_Config();
	 Ble_Config();
	 M433_Config(); 
	 
  
	 for(uint32_t i=0;i<=0xffff;i++);
	// App_Init();
	 TIM14_Config(App_10ms_Refresh);
}



