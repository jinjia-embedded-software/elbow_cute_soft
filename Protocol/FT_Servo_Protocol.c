#include "FT_Servo_Protocol.h"

void Delay_485(uint16_t ms)
{
   while(ms--)
	 {
	    for(uint32_t i=0;i<=0xffff;i++);
	 }
}

/*COM3 485模式发送数据
 *buf[]为发送的内容
 *len为发送的长度
 *out_time为等待返回的超时时间
*/
static uint8_t* FT_485_Feedback_Send(uint8_t buf[],uint16_t len,uint16_t out_time)
{
		 #define com com1
	
	   com->rx_state=0;	  
	   COM1_Dir_Send(buf, len);
		
		 //等待485返回数据
		 uint16_t wait_time=0;
		 while(com->rx_state==0)
		 {  
					Delay_485(1);
					wait_time++;
					if(wait_time>out_time)
					{
							break;
					}
		 }
		 
		 if(com->rx_state==1)
		 {  
				return com->rx_buf;
		 }
		 else
     {			 
		    return NULL;
		 }			 
    
    #undef com		 
}

/*FT舵机写入数据
 *id为需要写入舵机的ID号
 *para是写入数据的缓冲区
 *para_len需要写入数据的长度
 *out_time为等待返回的超时时间
*/
uint8_t* FT_Servo_Write_Data(uint8_t id,uint8_t para[],uint8_t para_len,uint16_t out_time)
{   
	  uint8_t cmd_len=para_len+6; //命令的长度
    if(cmd_len>128)
			 return NULL;
	  uint8_t cmd_buf[128]={0xFF,0xFF,id,(para_len+2),0x03};
		uint8_t idx=5;
		
		uint8_t sum=0;
		
		for(uint8_t i=0;i<para_len;i++)
		{
		    cmd_buf[idx++]=para[i];
		}
		 
		for(uint8_t i=2;i<(cmd_len-1);i++)
		{ 
				sum+=cmd_buf[i];
		}
		sum=~sum; //取反
		cmd_buf[cmd_len-1]=sum;	
		
		return FT_485_Feedback_Send(cmd_buf,cmd_len,out_time);
}


/*FT舵机同步写入数据
 *addr 是写入的地址
 *data_len需要写入数据的长度
 *id_table是电机id号的缓冲
 *data_table需要写入数据的缓冲
 *num是需要写入的电机数量
*/
void FT_Servo_Sync_Write_Data(uint8_t addr,uint8_t data_len,uint8_t id_table[],uint8_t data_table[],uint8_t num)
{   
	  uint16_t cmd_len=(data_len+1)*num+8;
	  if(cmd_len>256)
			 return ;
		
	  uint8_t cmd_buf[256]={0xFF,0xFF,0xFE,((data_len+1)*num+4),0x83,addr,data_len};
		uint8_t idx=7;
		uint8_t data_idx=0;
		uint8_t sum=0;	
		
		for(uint8_t i=0;i<num;i++)
	  {
		     cmd_buf[idx++]=id_table[i];//id号
			
			   for(uint8_t j=0;j<data_len;j++)
			   {
				     cmd_buf[idx++]=data_table[data_idx++];
				 }
		}
		 
		for(uint8_t i=2;i<(cmd_len-1);i++)
		{ 
				sum+=cmd_buf[i];
		}
		sum=~sum; //取反
		cmd_buf[cmd_len-1]=sum;
		
		COM1_Dir_Send(cmd_buf,cmd_len);
}


/*FT舵机读取数据
 *id是电机的id号
 *addr是读取数据的起始地址
 *len是需要读取数据的长度
 *返回数据是临时开辟内存，用完需要释放
*/
uint8_t* FT_Servo_Read_Data(uint8_t id,uint8_t addr,uint8_t len,uint16_t out_time)
{  
	  uint8_t cmd_buf[8]={0xFF,0xFF,id,0x04,0x02,addr,len};
		
		uint8_t sum=0;
		for(uint8_t i=2;i<7;i++)
		{ 
				sum+=cmd_buf[i];
		}
		sum=~sum; //取反
		cmd_buf[7]=sum;
		
		return FT_485_Feedback_Send(cmd_buf,8,out_time);
}


