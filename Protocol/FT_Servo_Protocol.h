#ifndef __FT_SERVO_Protocol_H
#define __FT_SERVO_Protocol_H

#include "stm32f0xx.h"
#include "RS485_CTL.h"
#include "COMM2.h"


/*对外函数*/
uint8_t* FT_Servo_Write_Data(uint8_t id,uint8_t para[],uint8_t para_len,uint16_t out_time);//FT舵机写入数据
void FT_Servo_Sync_Write_Data(uint8_t addr,uint8_t data_len,uint8_t id_table[],uint8_t data_table[],uint8_t num);//FT舵机同步写入数据
uint8_t* FT_Servo_Read_Data(uint8_t id,uint8_t addr,uint8_t len,uint16_t out_time);//FT舵机读出数据
#endif
