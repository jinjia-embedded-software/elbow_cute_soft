#include "Multi_Joint_Protocol.h"


/*关节电机原点设置*/
int8_t Joint_Set_Origin(uint8_t id)
{  
		uint8_t servo_write_buf[2]={40,128}; //设置原点发送给舵机的数据
		uint8_t *p=FT_Servo_Write_Data(id,servo_write_buf,2,30);
		if(p==NULL)
		{
				return -1;
		}
		return 0;	
}

/*关节电机使能/失能*/
int8_t Joint_Set_Enable(uint8_t id,uint8_t state)
{   
		uint8_t servo_write_buf[2]={40,state};		 			
		uint8_t *p=FT_Servo_Write_Data(id,servo_write_buf,2,10);
		if(p==NULL)
		{
				return -1;
		}
		return 0;		
}


/*关节绝对位置定位*/
int8_t Motor_Absolute_Pos(uint8_t id,uint16_t angle,uint16_t speed)
{    
	  uint8_t data[7];
	  uint8_t idx=0;
	  
	  data[idx++]=42;
		data[idx++]=angle; //低字节
		data[idx++]=angle>>8; //高字节	   
		//运行时间
		data[idx++]=0;
		data[idx++]=0;
		//运行速度
		data[idx++]=speed; //低字节
		data[idx++]=speed>>8; //高字节
	  uint8_t *p=FT_Servo_Write_Data(id,data,6,10);
		if(p==NULL)
		{
				return -1;
		}
		return 0;	
}




